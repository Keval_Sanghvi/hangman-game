// Variables
let level;
const keys = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
let letter;
const easy = ["java", "ruby", "class", "happy", "coding", "strings", "arrays", "number", "print", "plane"];
const medium = ["javascript", "jquery", "function", "python", "haskell", "register", "structure", "operator", "exception", "package"];
const hard = ["programming", "language", "frameworks", "expression", "compilation", "algorithm", "parameters", "variables", "operands", "architecture"];
let word;
let answer = "";
let remainingLives = 10;
let ctx = $("#myCanvas")[0].getContext("2d");

String.prototype.replaceAt = function (index, char) {
    let a = this.split("");
    a[index] = char;
    return a.join("");
}

function common() {
    $("#levelSelect").show();
    $("#keyboard").hide();
    $("#lives").hide();
    $("#guess").hide();
    $(".buttons").hide();
    $("#status").hide();
    $("#myCanvas").hide();
    $("#levelSelect p")[0].innerHTML = "GAME OVER!";
    $("#levelSelect").append("<button class='btn' id='refresh'>Play Again</button>")
    $("#msg").css("margin", "3rem");
    $("#refresh").click(function () {
        document.location.reload(true);
    });
}

function draw() {
    if (remainingLives == 9) {
        ctx.moveTo(10, 250);
        ctx.lineTo(200, 250);
        ctx.stroke();
    } else if (remainingLives == 8) {
        ctx.moveTo(60, 250);
        ctx.lineTo(60, 50);
        ctx.stroke();
    } else if (remainingLives == 7) {
        ctx.lineTo(160, 50);
        ctx.stroke();
    } else if (remainingLives == 6) {
        ctx.lineTo(160, 75);
        ctx.stroke();
    } else if (remainingLives == 5) {
        ctx.moveTo(180, 95);
        ctx.arc(160, 95, 20, 0, Math.PI * 2, true);
        ctx.moveTo(170, 89);
        ctx.arc(168, 89, 3, 0, Math.PI * 2, true);
        ctx.moveTo(155, 89);
        ctx.arc(152, 89, 3, 0, Math.PI * 2, true);
        ctx.moveTo(169, 106);
        ctx.arc(160, 106, 9, 0, Math.PI, true);
        ctx.stroke();
    } else if (remainingLives == 4) {
        ctx.moveTo(160, 115);
        ctx.lineTo(160, 200);
        ctx.stroke();
    } else if (remainingLives == 3) {
        ctx.moveTo(160, 125);
        ctx.lineTo(120, 150);
        ctx.stroke();
    } else if (remainingLives == 2) {
        ctx.moveTo(160, 125);
        ctx.lineTo(200, 150);
        ctx.stroke();
    } else if (remainingLives == 1) {
        ctx.moveTo(160, 200);
        ctx.lineTo(130, 220);
        ctx.stroke();
    } else if (remainingLives == 0) {
        ctx.moveTo(160, 200);
        ctx.lineTo(190, 220);
        ctx.stroke();
    }
}

function winLose() {
    if (!word.includes(letter)) {
        $("#status").text("Wrong Guess!").css("color", "red");
        remainingLives--;
        draw();
        if (remainingLives > -1) {
            $("#lives").text("Remaining Lives: " + remainingLives);
        }
        if (remainingLives == 0) {
            endGame();
        }
    }
    for (let i = 0; i < word.length; i++) {
        if (letter === word[i]) {
            $("#status").text("Correct Guess!").css("color", "green");
            answer = answer.replaceAt(i * 2, letter);
            $("#guess p").text(answer);
            check();
        }
    }
}

function check() {
    if (!answer.includes("_")) {
        setTimeout(function () {
            common();
            $("#levelSelect p")[1].innerHTML = "You Win!";
        }, 1000);
    }
}

function endGame() {
    setTimeout(function () {
        common();
        $("#levelSelect p")[1].innerHTML = "You Lose!";
    }, 1000);
}

$(function () {
    $(".btn").click(function () {
        if ($(this).text() == "Easy") {
            level = 1;
            word = easy[Math.floor(Math.random() * easy.length)];
        } else if ($(this).text() == "Medium") {
            level = 2;
            word = medium[Math.floor(Math.random() * medium.length)];
        } else if ($(this).text() == "Hard") {
            level = 3;
            word = hard[Math.floor(Math.random() * hard.length)];
        }
        $("p.hide").addClass("show");
        $("p.hide").removeClass("hide");
        $(".btn").off("click");
        $("#levelSelect").delay(2000).fadeOut(1000);
        setTimeout(function () {
            $("#lives").text("Remaining Lives: " + remainingLives);
            $("#lives").addClass("show");
            $("#lives").removeClass("hide");
            $("#myCanvas").show();
            for (let i = 0; i < 26; i++) {
                $("#keyboard").append("<button class='btn'>" + keys[i] + "</button>");
            }
            for (let i = 0; i < word.length; i++) {
                answer += "_ ";
            }
            $("#guess p").text(answer);
            $("#keyboard button").click(function () {
                letter = $(this)[0].innerHTML;
                winLose();
            });
            $(document).keypress(function (e) {
                if (e.keyCode == 32 || e.keyCode == 13) {
                    e.preventDefault();
                }
                if (e.keyCode >= 97 && e.keyCode <= 122) {
                    letter = String.fromCharCode(e.which);
                    winLose();
                }
            });
        }, 3000);
    });
});
